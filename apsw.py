#!/usr/bin/env python

# https://rogerbinns.github.io/apsw/index.html
# See the accompanying LICENSE file.

# APSW test suite - runs under both Python 2 and Python 3 hence a lot
# of wierd constructs to be simultaneously compatible with both.
# (2to3 is not used).

import apsw
import sys
import os
import codecs
import pprint

write=sys.stdout.write

def print_version_info(write=write):
    write("                Python "+sys.executable+" "+str(sys.version_info)+"\n")
    write("Testing with APSW file "+apsw.__file__+"\n")
    write("          APSW version "+apsw.apswversion()+"\n")
    write("    SQLite lib version "+apsw.sqlitelibversion()+"\n")
    write("SQLite headers version "+str(apsw.SQLITE_VERSION_NUMBER)+"\n")
    write("    Using amalgamation "+str(apsw.using_amalgamation)+"\n")

    if [int(x) for x in apsw.sqlitelibversion().split(".")]<[3,7,8]:
        write("You are using an earlier version of SQLite than recommended\n")

    sys.stdout.flush()

print_version_info()

connection=apsw.Connection("dbfile")
cursor=connection.cursor()
cursor.execute("drop table foo; drop table bar")
cursor.execute("create table foo(x,y,z)")

cursor.execute("insert into foo values(?,?,?)", (1, 1.1, None))  # integer, float/real, Null
cursor.execute("insert into foo(x) values(?)", ("abc", ))        # string (note trailing comma to ensure tuple!)
cursor.execute("insert into foo(x) values(?)",                   # a blob (binary data)
                            (b"abc\xff\xfe", ))                   # Use b"abc\xff\xfe" for Python 3

cursor.execute("delete from foo; insert into foo values(1,2,3); create table bar(a,b,c) ; insert into foo values(4, 'five', 6.0)")
for x,y,z in cursor.execute("select x,y,z from foo"):
        print (cursor.getdescription())  # shows column names and declared types
        print (x,y,z)


