#!/usr/bin/env python

# See the accompanying LICENSE file.

# APSW test suite - runs under both Python 2 and Python 3 hence a lot
# of wierd constructs to be simultaneously compatible with both.
# (2to3 is not used).

import apsw
import sys
import os
import codecs
import pprint
import cProfile

def inserts_tx(count):
    connection=apsw.Connection("dbfile")
    cursor=connection.cursor()
    cursor.execute("drop table NS;")
    cursor.execute("create table NS(id,domain,ns,IPs)")
    cursor.execute("begin transaction")
    for i in range(0,count):
        cursor.execute("insert into NS values(?,?,?,?)", (i,i,i,i))
    cursor.execute("commit")

def inserts_ntx(count):
    connection=apsw.Connection("dbfile")
    cursor=connection.cursor()
    cursor.execute("drop table NS;")
    cursor.execute("create table NS(id,domain,ns,IPs)")
    for i in range(0,count):
        cursor.execute("insert into NS values(?,?,?,?)", (i,i,i,i))

connection=apsw.Connection("dbfile")
cursor=connection.cursor()
cursor.execute("create table NS(id,domain,ns,IPs)")
for i in (1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 2000000, 5000000):
#for i in (100, 1000):
    print ("%s inserts inside transaction  " % i, end = ' : ')
    cProfile.run('inserts_tx(%s)' % i)
    print ("%s inserts outside transaction " % i, end = ' : ')
    cProfile.run('inserts_ntx(%s)' % i)
